'use strict'

/*
|--------------------------------------------------------------------------
| Providers
|--------------------------------------------------------------------------
|
| Providers are building blocks for your Adonis app. Anytime you install
| a new Adonis specific package, chances are you will register the
| provider here.
|
*/
const providers = [
  '@adonisjs/framework/providers/AppProvider',
  '@adonisjs/auth/providers/AuthProvider',
  '@adonisjs/bodyparser/providers/BodyParserProvider',
  '@adonisjs/cors/providers/CorsProvider',
  '@adonisjs/lucid/providers/LucidProvider',
  'adonis-algoliasearch/providers/AlgoliaSearchProvider',
  '@adonisjs/redis/providers/RedisProvider',
  '@adonisjs/validator/providers/ValidatorProvider',
  'adonis-bumblebee/providers/BumblebeeProvider',
  '@adonisjs/mail/providers/MailProvider',
  '@rocketseat/adonis-bull/providers/Bull',
  'adonis-acl/providers/AclProvider',
  'adonis-notifications/providers/NotificationsProvider',
  '@adonisjs/framework/providers/ViewProvider', // need for mailing
  '@adonisjs/websocket/providers/WsProvider', // need for chat
  '@adonisjs/drive/providers/DriveProvider' // need for S3
]

/*
|--------------------------------------------------------------------------
| Ace Providers
|--------------------------------------------------------------------------
|
| Ace providers are required only when running ace commands. For example
| Providers for migrations, tests etc.
|
*/
const aceProviders = [
  '@adonisjs/lucid/providers/MigrationsProvider',
  'adonis-bumblebee/providers/CommandsProvider',
  'adonis-acl/providers/CommandsProvider',
  'adonis-notifications/providers/CommandsProvider',
  '@adonisjs/vow/providers/VowProvider'
]

/*
|--------------------------------------------------------------------------
| Aliases
|--------------------------------------------------------------------------
|
| Aliases are short unique names for IoC container bindings. You are free
| to create your own aliases.
|
| For example:
|   { Route: 'Adonis/Src/Route' }
|
*/
const aliases = {}

/*
|--------------------------------------------------------------------------
| Commands
|--------------------------------------------------------------------------
|
| Here you store ace commands for your package
|
*/
const commands = []

const jobs = [
  'App/Jobs/Example'
]

module.exports = { providers, aceProviders, aliases, commands, jobs }
