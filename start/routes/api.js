'use strict'
/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
  Route.get('/', 'Api/User/ProfileController.me')//Done
  Route.resource('/adverts', 'Api/User/AdvertController')
  Route.resource('/advert-images', 'Api/User/AdvertImageController')
  //Route.resource('chats', 'Api/User/ChatController')
  Route.resource('/favorites', 'Api/User/FavoriteController')//Done
  Route.post('/locations', 'Api/User/LocationLogController.store')//Done
  Route.post('/report-abuse', 'Api/User/ReportAbuseController.store')//Done
})
  .prefix('v1/me')
  .middleware(['auth' , 'is:customer'])

Route.group(() => {
  Route.get('/search', 'Api/SearchController.search')
  Route.resource('/', 'Api/AdvertController').only(['index', 'show'])
})
  .prefix('v1/adverts')
  .middleware(['auth' , 'is:customer'])

Route.group(() => {
  // Route.get('/dispatch', 'UserController.dispatch')
  //Route.get('/users', 'Api/UserController.index')
  Route.get('/provinces', 'Api/LocationController.provinces')//Done
  Route.get('/districts/:id', 'Api/LocationController.districts')//Done
  Route.get('/sub-districts/:id', 'Api/LocationController.subDistricts')//Done
  Route.get('/car-makes', 'Api/CarMakeController.index')//Done
  Route.get('/car-models/:id', 'Api/CarModelController.index')//Done
})
  .prefix('v1')
  .middleware(['auth' , 'is:customer'])

Route.group(() => {
  Route.post('admin/login', 'Api/AuthController.adminLogin')//Done
  Route.post('/login', 'Api/AuthController.login')//Done
  Route.post('/register', 'Api/AuthController.register')// todo : need extra information
})
  .prefix('v1')

/*Route.group(() => {
  Route.post('/', 'RoomController.create')

  Route.get(':id', 'RoomController.select')
  Route.post(':id', 'RoomController.createMessage')
})
  .prefix('rooms')*/


Route.get('/', () => ({greeting: 'Chobbe API v1'}))
