'use strict'
/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
  Route.get('/', 'Api/User/ProfileController.me')
})
  .prefix('v1/admin/me')
  .middleware(['auth' , 'is:administrator'])

Route.group(() => {
  Route.get('/search', 'Api/SearchController.search')
  Route.resource('/', 'Api/AdvertController').only(['index', 'show'])
})
  .prefix('v1/admin/adverts')
  .middleware(['auth' , 'is:administrator'])

Route.group(() => {
  Route.post('/login', 'Api/AuthController.adminLogin')//Done
})
  .prefix('v1/admin')
