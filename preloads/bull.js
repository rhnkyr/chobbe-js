const Bull = use('Rocketseat/Bull')

Bull.process()

// Optionally you can start BullBoard:
Bull.ui()
// http://localhost:9999
