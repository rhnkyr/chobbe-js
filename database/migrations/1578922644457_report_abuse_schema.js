'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReportAbuseSchema extends Schema {
  up () {
    this.create('report_abuses', table => {
      table.bigIncrements().unsigned()
      table.bigInteger('user_id').unsigned()
      table.bigInteger('advert_id').unsigned()
      table.text('reason')
      table.timestamps()
    })
  }

  down () {
    this.drop('report_abuses')
  }
}

module.exports = ReportAbuseSchema
