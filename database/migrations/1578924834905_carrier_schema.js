'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CarrierSchema extends Schema {
  up () {
    this.create('carriers', table => {
      table.increments()
      table.string('name')
      table.specificType('status', 'tinyint(1)').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('carriers')
  }
}

module.exports = CarrierSchema
