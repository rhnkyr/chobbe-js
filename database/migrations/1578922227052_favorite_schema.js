'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FavoriteSchema extends Schema {
  up () {
    this.create('favorites', table => {
      table.bigIncrements().unsigned()
      table.bigInteger('user_id').unsigned()
      table.bigInteger('advert_id').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('favorites')
  }
}

module.exports = FavoriteSchema
