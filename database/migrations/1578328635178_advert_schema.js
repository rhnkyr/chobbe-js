'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AdvertSchema extends Schema {
  up () {
    this.create('adverts', table => {
      table.bigIncrements().unsigned()
      table.bigInteger('user_id').unsigned()
      table.integer('province_id').unsigned()
      table.integer('district_id').unsigned()
      table.integer('sub_district_id').unsigned()
      table.string('title')
      table.string('identifier')// cuid
      table.integer('category_id')
      table.json('options')
      table.text('description', 'longtext')
      table.specificType('location', 'point').notNullable().index('location', 'spatial')
      table.specificType('is_promoted', 'tinyint(1)').unsigned()
      table.specificType('status', 'tinyint(1)').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('adverts')
  }
}

module.exports = AdvertSchema
