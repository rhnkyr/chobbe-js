'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SpecialOfferSchema extends Schema {
  up () {
    this.create('special_offers', table => {
      table.bigIncrements().unsigned()
      table.bigInteger('advert_id').unsigned()
      table.bigInteger('to_user_id').unsigned()
      table.bigInteger('from_user_id').unsigned()
      table.specificType('is_valid', 'tinyint(1)').unsigned()
      table.decimal('price').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('special_offers')
  }
}

module.exports = SpecialOfferSchema
