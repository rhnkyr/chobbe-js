'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserLocationLogSchema extends Schema {
  up () {
    this.create('user_location_logs', table => {
      table.bigIncrements()
      table.bigInteger('user_id').unsigned()
      table.specificType('location', 'point').notNullable().index('location', 'spatial')
      table.timestamps()
    })
  }

  down () {
    this.drop('user_location_logs')
  }
}

module.exports = UserLocationLogSchema
