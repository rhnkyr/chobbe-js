'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrderSchema extends Schema {
  up () {
    this.create('orders', table => {
      table.bigIncrements().unsigned()
      table.bigInteger('user_id').unsigned()
      table.bigInteger('advert_id').unsigned()
      table.string('status')
      table.timestamps()
    })
  }

  down () {
    this.drop('orders')
  }
}

module.exports = OrderSchema
