'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaymentProviderSchema extends Schema {
  up () {
    this.create('payment_providers', table => {
      table.increments()
      table.string('name').comment('Payment provider name')
      table.string('key').comment('Payment provider api key').nullable()
      table.string('token').comment('Payment provider api token').nullable()
      table.specificType('is_valid', 'tinyint(1)').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('payment_providers')
  }
}

module.exports = PaymentProviderSchema
