'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TransactionLogSchema extends Schema {
  up () {
    this.create('transaction_logs', table => {
      table.bigIncrements().unsigned()
      table.bigInteger('user_id').unsigned()
      table.bigInteger('order_id').unsigned()
      table.decimal('amount').unsigned()
      table.string('type').comment('Keeps transaction type such as pending, finished, cancelled etc')
      table.integer('provider_id').comment('Bank  or payment provider id')
      table.string('transaction_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('transaction_logs')
  }
}

module.exports = TransactionLogSchema
