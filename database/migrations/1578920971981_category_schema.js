'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CategorySchema extends Schema {
  up () {
    this.create('categories', table => {
      table.increments().unsigned()
      table.string('name').notNullable().unique().index()
      table.integer('category_id').nullable().unsigned().references('id')
        .inTable('categories')
      table.string('lang')
      table.timestamps()
    })
  }

  down () {
    this.drop('categories')
  }
}

module.exports = CategorySchema
