'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AdvertImageSchema extends Schema {
  up () {
    this.create('advert_images', table => {
      table.increments()
      table.bigInteger('advert_id')
      table.string('path')
      table.integer('status').default(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('advert_images')
  }
}

module.exports = AdvertImageSchema
