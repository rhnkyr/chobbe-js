'use strict'

const Env = use('Env')

module.exports = {
  // redis connection
  connection: Env.get('BULL_CONNECTION', 'BULL')
}
