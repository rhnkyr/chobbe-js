const LogTypes = {
  ADVERT_CREATED: 1,
  ADVERT_UPDATED: 2,
  ADVERT_DELETED: 3,
}

module.exports = LogTypes
