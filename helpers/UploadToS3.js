const path = require('path')
const fs = require('fs')
const sharp = require('sharp')

const Drive = use('Drive')
const Helpers = use('Helpers')
const Logger = use('Logger')

/**
 * Upload to S3
 * @param advert_identifier
 * @param dim
 * @param file
 * @param folder
 * @param oldPath
 * @returns {Promise<{path: string, size: *, name: string, url: *}>}
 * @constructor
 */
const UploadToS3 = async (advert_identifier:string, dim:number, file, folder:string, oldPath:string) => {
  try {
    // If oldPath parameter is set then, delete the old picture
    if (oldPath) {
      const exists = await Drive.disk('s3').exists(oldPath)
      if (exists) {
        await Drive.disk('s3').delete(oldPath)
      }
    }

    // Create a random name for file
    const randomName = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
    //const fileName = `${randomName}_${Date.now()}.${file.subtype}`
    const fileName = `${randomName}_${Date.now()}.jpeg`

    // Sets the path and move the file
    const filePath = `${path.resolve(`./tmp/${folder}/`)}/${fileName}`
    await file.move(Helpers.tmpPath(folder), {name: fileName, overwrite: true})

    const resizedFilePath = `${path.resolve(`./tmp/${folder}/`)}/r_${fileName}`

    await sharp(filePath)
      .resize(dim, dim, {
        fit: 'contain',
        background: {r: 255, g: 255, b: 255, alpha: 1}
      })
      .jpeg({quality: 80})
      .toFormat('jpeg')
      .toFile(resizedFilePath)

    // Creates a readable stream from file and stores its size
    const fileStream = await fs.createReadStream(resizedFilePath)
    const fileSize = await file.stream.byteCount

    // Uploads the file to Amazon S3 and stores the url
    const s3Path = `${folder}/${advert_identifier}/${fileName}`
    await Drive.disk('s3').put(s3Path, fileStream, {ACL: 'public-read', ContentType: `${file.type}/${file.subtype}`})
    const fileUrl = await Drive.disk('s3').getUrl(s3Path)

    // Destroy the readable stream and delete the file from tmp path
    await fileStream._destroy()

    await Drive.delete(filePath)
    await Drive.delete(resizedFilePath)

    return {
      name: fileName,
      path: s3Path,
      size: fileSize,
      url: fileUrl
    }
  } catch (e) {
    Logger.transport('file').error(`Upload To S3 : ${e.message}`)
  }
}

module.exports = UploadToS3
