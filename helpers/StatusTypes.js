const StatusTypes = {
  WAITING_FOR_APPROVAL: 0,//Onay bekliyor
  APPROVED: 10,// Onaylandi
  NOT_SUITABLE: 20,// Uygun degil
  CANCELLED: 30,// Iptal Edildi
  WAITING_FOR_PAYMENT: 80,// Odeme bekliyor
  EXPIRED: 90,// Suresi gecti
  DELETED: 100,// Silindi
}

module.exports = StatusTypes
