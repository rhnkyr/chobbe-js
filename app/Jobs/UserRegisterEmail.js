'use strict'

const Mail = use('Mail');
const Env = use('Env')

class UserRegisterEmail {
  static get key() {
    return "UserRegisterEmailJob";
  }

  async handle(job) {

    const { data } = job;

    await Mail.send("emails.welcome", data, message => {
      message
        .from(Env.get('MAIL_USERNAME'),'Chobbe')
        .to(data.email, data.username)
        .subject("Welcome to Chobbe!");
    });

    return data;
  }
}

module.exports = UserRegisterEmail;

