'use strict'
const AlgoliaSearch = use('AlgoliaSearch')
const Env = use('Env')

class AddToIndex {
  static get key() {
    return "AddToIndex";
  }

  async handle(job) {

    const { data } = job;

    const index = AlgoliaSearch.index(Env.get('ALGOLIA_APP_INDEX'))

    const prepare = {
      objectID : data.identifier,
      city_en: data.province.name_in_english,
      city_th: data.province.name_in_thai,
      district_en: data.district.name_in_english,
      district_th: data.district.name_in_thai,
      sub_district_en: data.subDistrict.name_in_english,
      sub_district_th: data.subDistrict.name_in_thai,
      title : data.title,
      options : data.options,
      _geoloc : {
        lat : parseFloat(data.subDistrict.latitude),
        lng: parseFloat(data.subDistrict.longitude),
      },
      category_id : data.category_id,
      is_promoted : data.is_promoted
    }

    index.saveObject(prepare);

  }
}

module.exports = AddToIndex;

