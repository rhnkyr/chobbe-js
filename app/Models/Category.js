'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Category extends Model {
  categories () {
    return this.hasMany('App/Models/Category')
  }

  childrenCategories () {
    return this.hasMany('App/Models/Category').with('categories')
  }
}

module.exports = Category
