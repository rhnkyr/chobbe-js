'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Province extends Model {
  districts() {
    return this.hasMany('App/Models/District')
  }
}

module.exports = Province
