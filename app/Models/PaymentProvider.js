'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PaymentProvider extends Model {
}

module.exports = PaymentProvider
