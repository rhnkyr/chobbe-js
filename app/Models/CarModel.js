'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CarModel extends Model {
  brand() {
    return this.belongsTo('App/Models/CarBrand')
  }
}

module.exports = CarModel
