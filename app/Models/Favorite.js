'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Favorite extends Model {
  user () {
    return this.belongsTo('App/Models/User')
  }

  adverts(){
    return this.hasMany('App/Models/Advert','advert_id','id')
  }

}

module.exports = Favorite
