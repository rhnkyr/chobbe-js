'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class AdvertImage extends Model {
  advert () {
    return this.belongsTo('App/Models/Advert')
  }
}

module.exports = AdvertImage
