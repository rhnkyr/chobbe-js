'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Log = use('App/Models/Log')
const LogTypes = require('undot')('helpers/LogTypes')

class Advert extends Model {

  static boot() {
    super.boot()
    this.addHook('afterCreate', async instance => {

      const data = {
        title: instance.title,
        category_id: instance.category_id,
        options: instance.options,
        description: instance.description,
        location: instance.location.sql,
        is_promoted: instance.is_promoted,
        status: instance.status,
      }

      await Log.create({
        who_id: instance.user_id,
        type: LogTypes.ADVERT_CREATED,
        data: JSON.stringify(data)
      })
    })

    this.addHook('afterUpdate', async instance => {
      await Log.create({
        who_id: instance.user_id,
        type: LogTypes.ADVERT_UPDATED,
        data: JSON.stringify(instance.dirty)
      })
    })

    this.addHook('beforeDelete', async instance => {
      await Log.create({
        who_id: instance.user_id,
        type: LogTypes.ADVERT_DELETED,
        data: JSON.stringify({content_id: instance.id, content_type: 'advert'})
      })
    })
  }

  static get dates() {
    return super.dates.concat(['date', 'end_date'])
  }

  static castDates(field, value) {
    return value.format('DD-MM-YYYY')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

  images() {
    return this.hasMany('App/Models/AdvertImage')
  }

  province() {
    return this.belongsTo('App/Models/Province')
  }

  district() {
    return this.belongsTo('App/Models/District')
  }

  subDistrict() {
    return this.belongsTo('App/Models/SubDistrict')
  }
}

module.exports = Advert
