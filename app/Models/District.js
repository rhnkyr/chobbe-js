'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class District extends Model {
  subDistricts() {
    return this.hasMany('App/Models/SubDistrict')
  }
}

module.exports = District
