'use strict'

const CarMake = use('App/Models/CarMake')
const cuid = require('cuid')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with carmakes
 */
class CarMakeController {
  /**
   * Show a list of all carmakes.
   * GET carmakes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ transform }) {
    // return cuid()

    const makers = await CarMake.query().where('status', 1).fetch()
    // const makers = await CarMake.all()

    // https://github.com/rhwilr/adonis-bumblebee
    return transform.collection(makers, maker => ({
      id: maker.id,
      brand: maker.make
    }))
  }
}

module.exports = CarMakeController
