'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Advert = use('App/Models/Advert')
const moment = require('moment')
/**
 * Resourceful controller for interacting with advertises
 */
class AdvertController {
  /**
   * Show a list of all advertises.
   * GET advertises
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request }) {
    return Advert
      .query()
      .select('id', 'name', 'slug')
      .where('date', '>', moment(new Date()).format('Y-M-D'))
      .orderByRaw(`st_distance(\`location\`, ST_GeomFromText('POINT(${request.input('lon')} ${request.input('lat')})'))`)
      .limit(50)
      .fetch()
    // .paginate(1, 10)
  }

  /**
   * Display a single advertise.
   * GET advertises/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }
}

module.exports = AdvertController
