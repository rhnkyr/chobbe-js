'use strict'

const CarModel = use('App/Models/CarModel')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with carmodels
 */
class CarModelController {
  /**
   * Show a list of all carmodels.
   * GET carmodels
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ params, transform, response }) {
    const models = CarModel.query().where('car_make_id', params.id).fetch()

    // https://github.com/rhwilr/adonis-bumblebee
    const transformed = await transform.collection(models, 'CarModelTransformer')

    return response.json({
      status: 'success',
      data: transformed
    })
  }
}

module.exports = CarModelController
