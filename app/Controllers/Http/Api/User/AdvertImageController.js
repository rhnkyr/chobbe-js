'use strict'

const uploadToS3 = require('undot')('helpers/UploadToS3')

class AdvertImageController {

  async store({auth, request, response}) {

    const user = await auth.getUser()
    const {id} = request.all()

    const advert = await user.adverts().where('identifier', id).first()

    const files = request.file('images', {
      types: ['image'],
      size: '2mb'
    })


    for (const file of files.files) {

      try {

        const uploaded = await uploadToS3(id, 960, file, 'adverts')

        await advert.images().create({path: uploaded.url, status: 0})

      } catch (error) {
        console.log(error)
      }

    }

    return response.json({
      status: 'success'
    })

  }

  async destroy({auth, request, response}) {
    const user = await auth.getUser()
    const {id} = request.all()

    await user.adverts().images().where('id', id).delete()

    return response.json({
      status: 'success'
    })
  }
}

module.exports = AdvertImageController
