'use strict'

// const Redis = use("Redis");
const Bull = use('Rocketseat/Bull')
const Job = use('App/Jobs/UserRegisterEmail')
// const collect = require('collect.js')

class ProfileController {

  /**
   * Me
   * @param auth
   * @param transform
   * @param response
   * @returns {Promise<*>}
   */
  async me ({ auth, transform, response }) {
    const transformed = await transform.item(auth.getUser(), 'UserTransformer')

    return response.json({
      status: 'success',
      data: transformed
    })
  }

  async dispatch () {
    const data = { username: 'Deneme' }

    await Bull.add(Job.key, data)
  }
}

module.exports = ProfileController
