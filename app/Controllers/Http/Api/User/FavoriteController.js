'use strict'

class FavoriteController {

  async index({auth, transform, response}) {
    const user = await auth.getUser()

    await user.load('favoriteAdverts.user')
    const favoriteAdverts = user.getRelated('favoriteAdverts')

    const transformed = await transform.collection(favoriteAdverts, 'UserFavoriteTransformer')

    return response.json({
      status: 'success',
      data: transformed
    })

  }

  async store({auth, request, response}) {

    const {advert_id} = request.all()

    const user = await auth.getUser()

    await user
      .favorites()
      .create({
        advert_id
      })

    return response.json({
      status: 'success'
    })

  }

  async destroy({auth, params, request, response}) {

    const user = await auth.getUser()

    await user
      .favorites()
      .where('advert_id', params.id)
      .delete()

    return response.json({
      status: 'success'
    })
  }
}

module.exports = FavoriteController
