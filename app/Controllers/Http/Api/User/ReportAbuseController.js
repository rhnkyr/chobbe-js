'use strict'

class ReportAbuseController {

  async store({auth, request, response}) {

    const {advert_id, reason} = request.all()

    const user = await auth.getUser()

    await user
      .reportAbuses()
      .create({
        advert_id,
        reason
      })

    return response.json({
      status: 'success'
    })

  }
}

module.exports = ReportAbuseController
