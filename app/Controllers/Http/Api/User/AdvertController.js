'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */

/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const cuid = require('cuid')

const Database = use('Database')
const Bull = use('Rocketseat/Bull')
const Job = use('App/Jobs/AddToIndex')
const Advert = use('App/Models/Advert')

const StatusTypes = require('undot')('helpers/StatusTypes')

class AdvertController {

  async index({auth, transform, response}) {

    const user = await auth.getUser()

    await user.load('adverts.images')

    const adverts = user.getRelated('adverts')

    const transformed = await transform.collection(adverts, 'UserAdvertTransformer')

    return response.json({
      status: 'success',
      data: transformed
    })

  }


  async store({auth, request, response}) {

    const user = await auth.getUser()

    let data = request.all();

    const points = data.location.split(',')

    const advert = await user
      .adverts()
      .create({
          ...data,
          identifier: cuid(),
          status: StatusTypes.WAITING_FOR_APPROVAL,
          options: JSON.stringify(data.options),
          location: Database.raw(`POINT(${points[0]},${points[1]})`)
        }
      )

    const clear = await Advert
      .query()
      .with('province')
      .with('district')
      .with('subDistrict')
      .where('identifier', advert.identifier)
      .first()

    await Bull.add(Job.key, clear.toJSON())

    return response.json({
      status: 'success',
      id: advert.identifier
    })

  }

  async show({auth, params, transform, response}) {
    const user = await auth.getUser()

    let advert

    try {
      advert = await user
        .advertsWithImages()
        .where('identifier', params.id)
        .firstOrFail()
    } catch (e) {
      return response.json({
        status: 'error',
        data: e.message
      })
    }

    const transformed = await transform.item(advert, 'UserAdvertTransformer')

    return response.json({
      status: 'success',
      data: transformed
    })
  }


  //Todo : Advert update
  async update({auth, params, request, response}) {

  }

  async destroy({auth, params, request, response}) {

    const user = await auth.getUser()

    await user
      .adverts()
      .where('identifier', params.id)
      .update({status: StatusTypes.DELETED})

    return response.json({
      status: 'success'
    })
  }
}

module.exports = AdvertController
