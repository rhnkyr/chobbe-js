'use strict'
const Database = use('Database')

class LocationLogController {
  async store({auth, request}) {
    const user = await auth.getUser()

    let {location} = request.all();

    const points = location.split(',')

    await user.locations().create({location: Database.raw(`POINT(${points[0]},${points[1]})`)})
  }
}


module.exports = LocationLogController
