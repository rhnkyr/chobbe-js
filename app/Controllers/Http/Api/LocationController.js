'use strict'

const Province = use('App/Models/Province')
const District = use('App/Models/District')
const SubDistrict = use('App/Models/SubDistrict')

class LocationController {

  async provinces({transform}) {

    const provinces = await Province.all()

    return transform.collection(provinces, province => ({
      id: province.id,
      th: province.name_in_thai,
      en: province.name_in_english
    }))
  }

  async districts({params, transform}) {
    const districts = await District.query().where('province_id', params.id).fetch()

    return transform.collection(districts, district => ({
      id: district.id,
      th: district.name_in_thai,
      en: district.name_in_english
    }))
  }

  async subDistricts({params, transform}) {
    const subDistricts = await SubDistrict.query().where('district_id', params.id).fetch()

    return transform.collection(subDistricts, sub_district => ({
      id: sub_district.id,
      th: sub_district.name_in_thai,
      en: sub_district.name_in_english
    }))
  }


}

module.exports = LocationController
