'use strict'

const AlgoliaSearch = use('AlgoliaSearch')

class SearchController {
  async search ({ request }) {
    const index = AlgoliaSearch.index('adverts')

    // /adverts-search?keyword=ปฏิทิน
    // const results = await index
    //  .search(request.input('keyword'))

    // adverts by user location
    // https://www.algolia.com/doc/guides/managing-results/refine-results/geolocation/how-to/filter-results-around-a-location/
    const results = await index
      .search({
        query: '', // or request.input('keyword')
        aroundLatLng: '40.71, -74.01',
        aroundRadius: 100000, // 100 km
        attributesToRetrieve: ['firstname', 'lastname'], // Mesela
        hitsPerPage: 50 // limit 50
      })

    return results
  }
}

module.exports = SearchController
