'use strict'

const User = use('App/Models/User')
const Bull = use('Rocketseat/Bull')
const Job = use('App/Jobs/UserRegisterEmail')

class AuthController {
  // Login for user
  async login({request, auth}) {
    const {email, password} = request.all()
    return await auth.attempt(email, password)
  }

  // Login for user
  async adminLogin({request, auth}) {
    const {email, password} = request.all()
    return await auth.query(function (query) {
      query.where('type', 10)
    }).attempt(email, password)
  }

  // Register for user
  async register({request, response}) {
    const {email, password} = request.all()
    const user = await User.create({
      email,
      password,
      username: email
    })

    await user.roles().attach([3])

    await Bull.add(Job.key, user)
    // return this.login(...arguments)
    return response.json({
      status: 'success',
      data: {message: 'Check your email to verify your account!'}
    })
  }
}

module.exports = AuthController
