'use strict'

const User = use('App/Models/User')

class UserController {
  async index ({ transform, response }) {
    /* const cachedUsers = await Redis.get('users');
    if (cachedUsers) {
      return JSON.parse(cachedUsers)
    } */

    const users = await User.all()

    // redis
    // await Redis.set('users', JSON.stringify(users)).expire(10);
    // collect.js
    // const filtered = collect(users.toJSON()).where('username', 'a@b.com');
    // return transform.collection(filtered.all(), 'UserTransformer'); //collect.js

    const transformed = await transform.collection(users, 'UserTransformer')

    return response.json({
      status: 'success',
      data: transformed
    })
  }
}

module.exports = UserController
