'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserAdvertTransformer class
 *
 * @class UserAdvertTransformer
 * @constructor
 */
class UserAdvertTransformer extends BumblebeeTransformer {
  static get defaultInclude() {
    return [
      'images'
    ]
  }

  /**
   * This method is used to transform the data.
   */
  transform(advert) {
    return {
      identifier: advert.identifier,
      name: advert.title,
      description: advert.description,
      category: advert.category_id,
      options: advert.options,
      location: advert.location,
      is_promoted: advert.is_promoted,
    }
  }

  includeImages(advert) {
    return this.collection(advert.getRelated('images'), 'ImageTransformer')
  }
}

module.exports = UserAdvertTransformer
