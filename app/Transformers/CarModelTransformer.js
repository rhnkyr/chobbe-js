'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * CarModelTransformer class
 *
 * @class CarModelTransformer
 * @constructor
 */
class CarModelTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data.
   */
  transform (model) {
    return {
      id: model.id,
      model: model.model
    }
  }
}

module.exports = CarModelTransformer
