'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserFavoriteTransformer class
 *
 * @class UserFavoriteTransformer
 * @constructor
 */
class UserFavoriteTransformer extends BumblebeeTransformer {
  static get defaultInclude () {
    return [
      'user'
    ]
  }
  /**
   * This method is used to transform the data.
   */
  transform(model) {
    //console.log(model)
    return {
      identifier: model.identifier,
      name: model.title
    }
  }

  includeUser (model) {
    return this.item(model.getRelated('user'), 'UserTransformer')
  }
}

module.exports = UserFavoriteTransformer
