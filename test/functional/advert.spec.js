'use strict'

const {test, trait} = use('Test/Suite')('Advert')

const Advert = use('App/Models/Advert')
const User = use('App/Models/User')

trait('Test/ApiClient')
trait('Auth/Client')

test('create advert', async ({client}) => {

  const user = await User.find(6)

  const data = {
    "title": "Deneme 3",
    "category_id": 1,
    "options": {
      "for": "sale",
      "type": "home",
      "area": "100",
      "room_count": "2",
      "price": "1000000",
      "usage_status": "used"
    },
    "description": "Description",
    "location": "17.2121,18.0000",
    "is_promoted": 0,
    "status": 0
  }

  const response = await client
    .post('/v1/me/adverts', data)
    .loginVia(user, 'jwt')
    .end()

  console.log(response)

  response.assertStatus(200)
  response.assertJSONSubset([{
    "title": "Deneme 3",
    "category_id": 1,
  }])
})
